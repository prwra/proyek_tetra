@extends('layout.admin_app')

@section('title', 'Admin | Rekapitulasi')

@section('content')
	<div class="col-12 text-center tit">
		<p><h3>Rekapitulasi Apa Yang Ingin Anda Lihat?</h3></p>
	</div>
	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-4 col-md-4 col-sm-12 col-xs-12 kiri">
				<picture>
					<img src="/img/rekap/satu.png" alt="" class="img-rek">
				</picture>
				<p><b><h4>Surat Tugas</h4></b></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore placeat quod vel nostrum totam nulla deserunt optio blanditiis,</p>
				<br><br>
				<a href="{{ url('admin/rekapitulasi/surat_tugas') }}" class="a-rek">LANJUT BACA</a>
			</div>
			<div class="col-4 col-md-4 col-sm-12 col-xs-12  tengah">
				<picture>
					<img src="/img/rekap/dua.png" alt="" class="img-rek">
				</picture>
				<p><b><h4>Perjalanan</h4></b></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore placeat quod vel nostrum totam nulla deserunt optio blanditiis,</p>
				<br><br>
				<a href="{{ url('admin/rekapitulasi/perjalanan') }}" class="a-rek">LANJUT BACA</a>
			</div>
			<div class="col-4 col-md-4 col-sm-12 col-xs-12  kanan">
				<picture>
					<img src="/img/rekap/tiga.png" alt="" class="img-rek">
				</picture>
				<p><b><h4>Keuangan</h4></b></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore placeat quod vel nostrum totam nulla deserunt optio blanditiis,</p>
				<br><br>
				<a href="{{ url('admin/rekapitulasi/keuangan') }}" class="a-rek">LANJUT BACA</a>
			</div>
		</div>
	</div>
@endsection
