<!-- Modal -->
  <div class="modal fade" id="profil" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title text-center">Profil Saya</h2>
        </div>
          <form action="#">
          	<div class="col-lg-12 col-sm-12">
			<div class="form-group">
			    <label for="text">Nama :</label>
			    <input type="text" class="form-control" id="nama" disabled>
			</div>
			</div>

          	<div class="col-lg-12 col-sm-12">
			<div class="form-group">
			    <label for="text">NIP :</label>
				<input type="text" class="form-control" name="" id="nip" disabled>
			</div>
			</div>

			<div class="col-lg-12 col-sm-12">
			<div class="form-group">
			    <label for="text">Jabatan :</label>
				<input type="text" class="form-control" name="" id="jabatan" disabled>
			</div>
			</div>

			<div class="col-lg-12 col-sm-12">
			<div class="form-group">
			    <label for="text">Golongan :</label>
				<input type="text" class="form-control" name="" id="golongan" disabled>
			</div>
			</div>

			<div class="col-lg-12 col-sm-12">
			<div class="form-group">
			    <label for="text">Eselon :</label>
				<input type="text" class="form-control" name="" id="eselon" disabled>
			</div>
			</div>
		  </form>
        <div class="modal-footer">
          	<div class="col-lg-12 col-sm-12">
	          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
        </div>
      </div>
    </div>
  </div>