@extends('layout.admin_app')

@section('title', 'Admin | Rekapitulasi | Perjalanan')

@section('content')
	<div class="container-fluid text-center">
		<h1 class="head"><a href="" class="head-ak">Rekapitulasi Perjalanan</a></h1>
	</div>
	
	<div class="container-fluid blue">
		@include('sort.wilayah') <!-- sorting -->

		@include('sort.urut') <!-- sorting -->

		<table border="1" class="bg-blue text-center">
			<tr>
				<td width="3%" rowspan="2">No.</td>
				<td width="40%" colspan="2">Surat Tugas</td>
				<td width="20%" rowspan="2">NIP Anggota</td>
				<td width="14%" rowspan="2">Tujuan</td>
				<td width="9" rowspan="2">Lama Perjalanan ( Hari )</td>
				<td width="9%" rowspan="2">Aksi</td>
			</tr>
			<tr>
				<td width="25%">Nomor SPPD</td>
				<td width="15%">Tanggal Pemberian</td>
			</tr>
			<tr>
				<td>1</td>
				<td>1001 Agung</td>
				<td>10 Januari 1998</td>
				<td>16523165</td>
				<td>Bengkulu</td>
				<td>7</td>
				<td>
					<button class="butn-det">
						Hapus
					</button>
				</td>
			</tr>
		</table>
	</div>
@endsection
