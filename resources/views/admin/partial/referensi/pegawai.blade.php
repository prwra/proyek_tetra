@extends('layout.admin_app')

@section('title', 'Admin | Pegawai')

@section('content')
	<div class="col-12 text-center tit">
		<p><h3>Referensi Pegawai</h3></p>
	</div>

	<div class="container-fluid blue">
		@include('sort.urut_nama')

		@include('admin.form.tambah_pegawai_form')
		

		<button class="navbar-right butn" id="add">
			Tambah Pegawai
		</button>

		<table border="1" class="bg-blue text-center" style="width:100%;">
			<tr>
				<th width="3%">No.</th>
				<th width="8%">NIP</th>
				<th width="17%">Nama</th>
				<th width="10%">Jenis Kelamin</th>
				<th width="15%">Alamat</th>
				<th width="10%">Jabatan</th>
				<th width="10%">Eselon</th>
				<th width="10%">Golongan</th>
				<th width="7%">Aksi</th>
			</tr>
			<?php $no = 0 ?>
			@foreach($pengguna as $p)
			<?php $no = $no + 1; ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$p->nip}}</td>
				<td>{{$p->nama}}</td>
				<td>{{$p->kelamin}}</td>
				<td>{{$p->alamat}}</td>
				<td>{{$p->jabatan}}</td>
				<td>{{$p->eselon}}</td>
				<td>{{$p->golongan}}</td>
				<td colspan="4" class="text-center">

					<form method="get" action="pegawai/{{$p->nip}}/edit">
					    <button type="submit" class="act-butn btn-warning" id="edt">Ubah</button>
					</form>
					<!--<button class="act-butn btn-warning" href="pegawai/{{$p->nip}}/edit" id="edt">Ubah</button>-->
					<!--<a class="act" href="pegawai/{{$p->nip}}/edit">Ubah</a>-->
					<form action="pegawai/{{$p->nip}}" method="POST">
						<input type="hidden" name="_method" value="delete">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input class="act-butn btn-danger" type="submit" name="submit" value="Hapus">
					</form>
				</td>
			</tr>
			@endforeach
		</table>
	</div>


<!-- JS for show popup -->
<script type="text/javascript">
	$('#add').on('click',function(){
		$('#tambah_pegawai').modal('show');
	})
	$('#edt').on('click',function(){
		$('#ubah_pegawai').modal('show');
	})
</script>
@endsection
