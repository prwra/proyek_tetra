@extends('layout.admin_app')

@section('title', 'Admin | Surat Tugas')

@section('content')
	<div class="container-fluid text-center">
		<h1 class="head"><a href="{{ url('admin/surat_tugas') }}" class="head-ak">Surat Tugas</a>
	</div>
	
	<div class="container-fluid bg-head text-center">
	<p> <!-- about page -->
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo numquam recusandae at molestiae officia, odio quae id provident! Laudantium quos, ea rem, animi soluta odit nulla natus repellendus blanditiis. Ducimus!
	</p>
	</div>

	<div class="container-fluid blue">
		@include('sort.wilayah')

		@include('sort.urut')

		@include('admin.form.tambah_surat_form')

		@include('admin.partial.detail_surat')

		<button class="navbar-right butn" id="add">
			Tambah Surat
		</button>

		<table border="1" class="bg-blue text-center">
			<tr>
				<td width="3%" rowspan="2">No.</td>
				<td width="37%" colspan="2">Surat Tugas</td>
				<td width="30%" colspan="2">Waktu Pelaksanaan</td>
				<td width="14%" rowspan="2">Tujuan</td>
				<td width="9%" rowspan="2">Status</td>
				<td width="15%" rowspan="2" colspan="3">Aksi</td>
			</tr>
			<tr>
				<td width="22%">Nomor SPPD</td>
				<td width="15%">Tanggal Pemberian</td>
				<td>Mulai</td>
				<td>Selesai</td>
			</tr>
			<?php $no = 0 ?>
			@foreach($surat_tugas as $s)
			<?php $no = $no + 1; ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$s->kode_SPPD}}</td>
				<td>{{$s->tgl_pemberian->format('d F Y')}}</td>
				<td>{{$s->tgl_mulai->format('d F Y')}}</td>
				<td>{{$s->tgl_selesai->format('d F Y')}}</td>
				<td>{{$s->lokasi}}</td>
				<td>{{$s->status}}</td>
				
				<td>
					<form method="get" action="surat_tugas/{{$s->kode_SPPD}}/lihat">
			      		<button class="act-butn btn-success" type="submit">Lihat</button>
			      	</form>
				</td>
				<td>
					<form method="get" action="surat_tugas/{{$s->kode_SPPD}}/edit">
					    <button type="submit" class="act-butn btn-warning" id="edt">Ubah</button>
					</form>
				</td>
				<td>
					<form action="surat_tugas/{{$s->kode_SPPD}}" method="POST">
						<input type="hidden" name="_method" value="delete">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input class="act-butn btn-danger" type="submit" name="submit" value="Hapus">
					</form>
				</td>
			</tr>
			@endforeach
		</table>
	</div>


<!-- JS for show popup -->
<script type="text/javascript">
	$('#add').on('click',function(){
		$('#tambah_surat').modal('show');
	});

	$('#detail').on('click',function(){
		$('#detail_surat').modal('show');
	});
</script>
@endsection
