<!-- Modal -->
  <div class="modal fade" id="tambah_surat" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title text-center">Tambah Surat Tugas</h2>
        </div>
          <form action="{{ route('surat_tugas.store') }}" method="POST">
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Kode SPPD :</label>
			    <input type="text" class="form-control" name="kode_SPPD" id="kode_SPPD">
			</div>
			</div>

          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Pemberian :</label>
				<input type="date" class="form-control" name="tgl_pemberian" id="tgl_pemberian">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Mulai :</label>
				<input type="date" class="form-control" name="tgl_mulai" id="tgl_mulai">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Selesai :</label>
				<input type="date" class="form-control" name="tgl_selesai" id="tgl_selesai">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Lokasi Tujuan :</label>
			    <input type="text" class="form-control" name="lokasi" id="lokasi">
			</div>
			</div>

          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="file">File :</label>
			    <input type="file" class="form-control" name="file" id="file">
			</div>
			</div>

			<!--<div id="field">
            <div id="field0">
			<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">NIP Anggota :</label>
			    <input type="text" class="form-control" name="anggota" id="anggota">
			</div>
			</div>
			</div>
			</div>-->
			<div class="modal-footer">
          	<div class="col-lg-12 col-sm-12">
    			<!--<button id="add-more" name="add-more" class="navbar-left btn btn-primary">Tambah Anggota</button>-->
    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	        	<input type="submit" class="btn btn-primary" value="Tambah">
	          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
        </div>
		  </form>
      </div>
    </div>
  </div>
 <!-- end of modal -->
 <script>
$(document).ready(function () {
    var next = 0;
    $("#add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><!-- Text input--><div class="col-md-6 col-sm-6"> <div class="form-group"><input type="text" class="form-control" name="anggota'+ next +'" id="anggota"></div></div>';
        var newInput = $(newIn);
        var removeBtn = '<div class="col-md-6 col-sm-6"> <div class="form-group"><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me navbar-right" >Remove</button></div></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });

});
 </script>
<!--
<form action="#">
	<div class="form-group">
	    <label for="text">Kode SPPD :</label>
	    <input type="text" class="form-control" id="kode">
	</div>
	<div class="form-group">
	    <label for="date">Tanggal Pemberian :</label>
		<input type="date" class="form-control" name="" id="tgl_pemberian">
	</div>
	<div class="form-group">
	    <label for="date">Tanggal Mulai :</label>
		<input type="date" class="form-control" name="" id="tgl_mulai">
	</div>
	<div class="form-group">
	    <label for="date">Tanggal Selesai :</label>
		<input type="date" class="form-control" name="" id="tgl_selesai">
	</div>
	<div class="form-group">
	    <label for="nbr">Lama Perjalanan (Hari) :</label>
	    <input type="number" class="form-control" id="lama_perjalanan">
	</div>
	<div class="form-group">
	    <label for="text">Tujuan :</label>
	    <input type="text" class="form-control" id="tujuan">
	</div>
	<label class="radio-inline"><input type="radio" name="optradio">Admin</label>
	<label class="radio-inline"><input type="radio" name="optradio">Pegawai</label>
	<br>
  	<button type="submit" class="btn btn-success">Submit</button>
  	<button type="submit" class="btn btn-danger">Reset</button>
</form>
-->
	