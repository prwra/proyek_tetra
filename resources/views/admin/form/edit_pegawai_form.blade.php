<!-- Modal -->
  <div class="modal fade" id="ubah_pegawai" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title text-center">Ubah Data Pegawai</h2>
        </div>
        <form action="{{route('pengguna.update', $p->nip)}}" method="POST">
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">NIP :</label>
			    <input type="text" class="form-control" id="nip" name="nip" value="{{$p->nip}}" required>
			</div>
			</div>

          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Nama :</label>
				<input type="text" class="form-control" name="nama" id="nama" value="{{$p->nama}}" required>
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Tempat Lahir :</label>
				<input type="text" class="form-control" name="tmpt_lahir" id="tmpt_lahir" value="{{$p->tempat_lahir}}">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Lahir :</label>
				<input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" value="{{$p->tgl_lahir}}">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="jk">Jenis Kelamin :</label><br>
				<label class="radio-inline"><input type="radio" name="jns_kel" value="Laki-Laki" checked>Laki-Laki</label>
				<label class="radio-inline"><input type="radio" name="jns_kel" value="Perempuan">Perempuan</label>
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Alamat :</label>
			    <textarea class="form-control" name="alamat"><?php echo $p->alamat ?></textarea>
			</div>
			</div>

			<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Jabatan :</label>
				<input type="text" class="form-control" name="jabatan" id="jabatan" value="{{$p->jabatan}}" required>
			</div>
			</div>

			<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Eselon :</label>
				<input type="text" class="form-control" name="eselon" id="eselon" value="{{$p->eselon}}" required>
			</div>
			</div>

			<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Golongan :</label>
				<input type="text" class="form-control" name="golongan" id="golongan" value="{{$p->golongan}}" required>
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			</div>

        	<div class="modal-footer">
	          	<div class="col-lg-12 col-sm-12">
	          		<input type="hidden" name="_method" value="put">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
		        	<input type="submit" class="btn btn-primary" name="submit" value="Ubah">
		          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
        	</div>
		</form>
      </div>
    </div>
  </div>