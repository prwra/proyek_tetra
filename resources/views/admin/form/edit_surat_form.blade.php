@extends('layout.user_app')

@section('title', 'Tetra | Ubah Surat Tugas')

@section('content')
<!-- Modal -->
        <div class="modal-header">
          <h2 class="modal-title text-center">Ubah Surat Tugas</h2>
          <form action="{{route('surat_tugas.update', $s->kode_SPPD)}}" method="POST" enctype="multipart/form-data">
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Kode SPPD :</label>
			    <input type="text" class="form-control" name="kode_SPPD" id="kode_SPPD" value="{{$s->kode_SPPD}}" required>
			</div>
			</div>

          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Pemberian :</label>
				<input type="date" class="form-control" name="tgl_pemberian" id="tgl_pemberian" value="{{$s->tgl_pemberian}}" required>
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Mulai :</label>
				<input type="date" class="form-control" name="tgl_mulai" id="tgl_mulai" value="{{$s->tgl_mulai}}">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="date">Tanggal Selesai :</label>
				<input type="date" class="form-control" name="tgl_selesai" id="tgl_selesai" value="{{$s->tgl_selesai}}">
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Lokasi Tujuan :</label>
			    <input type="text" class="form-control" name="lokasi" id="lokasi" value="{{$s->lokasi}}">
			</div>
			</div>

          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="file">File :</label>
			    <input type="file" class="form-control" name="file" id="file" accept="image/*">
			</div>
			</div>
		<div class="modal-footer">
          	<div class="col-lg-12 col-sm-12">
    			<!--<button id="add-more" name="add-more" class="navbar-left btn btn-primary">Tambah Anggota</button>-->
    			<input type="hidden" name="_method" value="put">
    			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	        	<input type="submit" class="btn btn-primary" value="Ubah">
	          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
        </div>
        </div>
		  </form>
		  <div class="container text-center">
		  <!--<img src="{{ asset('storage/upload/1.png')}}" alt=""> <!--dari ini-->
		  <img src="{{ asset('storage/upload/'.$s->file)}}" alt=""> <!--jadi kyk gini-->
		  </div>
 <!-- end of modal -->
@endsection
<script>
$(document).ready(function () {
    var next = 0;
    $("#add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><!-- Text input--><div class="col-md-6 col-sm-6"> <div class="form-group"><input type="text" class="form-control" name="anggota'+ next +'" id="anggota"></div></div>';
        var newInput = $(newIn);
        var removeBtn = '<div class="col-md-6 col-sm-6"> <div class="form-group"><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me navbar-right" >Remove</button></div></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });

});
 </script>
<!--
<form action="#">
	<div class="form-group">
	    <label for="text">Kode SPPD :</label>
	    <input type="text" class="form-control" id="kode">
	</div>
	<div class="form-group">
	    <label for="date">Tanggal Pemberian :</label>
		<input type="date" class="form-control" name="" id="tgl_pemberian">
	</div>
	<div class="form-group">
	    <label for="date">Tanggal Mulai :</label>
		<input type="date" class="form-control" name="" id="tgl_mulai">
	</div>
	<div class="form-group">
	    <label for="date">Tanggal Selesai :</label>
		<input type="date" class="form-control" name="" id="tgl_selesai">
	</div>
	<div class="form-group">
	    <label for="nbr">Lama Perjalanan (Hari) :</label>
	    <input type="number" class="form-control" id="lama_perjalanan">
	</div>
	<div class="form-group">
	    <label for="text">Tujuan :</label>
	    <input type="text" class="form-control" id="tujuan">
	</div>
	<label class="radio-inline"><input type="radio" name="optradio">Admin</label>
	<label class="radio-inline"><input type="radio" name="optradio">Pegawai</label>
	<br>
  	<button type="submit" class="btn btn-success">Submit</button>
  	<button type="submit" class="btn btn-danger">Reset</button>
</form>
-->
	