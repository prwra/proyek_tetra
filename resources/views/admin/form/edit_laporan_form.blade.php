<!-- Modal -->
  <div class="modal fade" id="ubah_laporan" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title text-center">Ubah Laporan</h2>
        </div>
          <form action="{{route('laporan.update', $l->kode_laporan)}}" method="POST">
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Kode SPPD :</label>
			    <input type="text" class="form-control" name="kode_SPPD" id="kode" value="{{$l->kode_SPPD}}" required>
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="text">Total Biaya :</label>
			    <input type="text" class="form-control" name="biaya" id="biaya" placeholder="contoh : 100000" value="{{$l->biaya}}" required>
			</div>
			</div>
			
          	<div class="col-lg-6 col-sm-6">
			<div class="form-group">
			    <label for="file">File :</label>
			    <input type="file" class="form-control" name="file" id="file">
			</div>
			</div>
			<div class="modal-footer">
          	<div class="col-lg-12 col-sm-12">
          		<input type="hidden" name="_method" value="put">
          		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	        	<input type="submit" class="btn btn-primary" value="Ubah">
	          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
        	</div>
		  </form>
      </div>
    </div>
  </div>