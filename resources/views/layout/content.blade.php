<style>
	img {
		width:100%;
	}
</style>
<div class="container-fluid">
	<div class="row">
	    <div class="col-lg-6 col-md-5 col-sm-5 col-xs-5">
		    <h1>Ajukan Surat Tugas</h1>
		    <p><a href="{{ url('home/surat_tugas') }}">LIHAT LEBIH DETAIL</a></p>
	    </div>
	    <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7">
		    <picture>
		    	<img src="/img/home/satu.png" alt="">
		    </picture>
	    </div>
	</div>
	<div class="row">
	    <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7">
	      <picture>
	      	<img src="/img/home/dua.png" alt="">
	      </picture>
	    </div>
	    <div class="col-lg-6 col-md-5 col-sm-5 col-xs-5">
	      <h1>Ajukan Laporan SPPD</h1>
	      <p><a href="{{ url('home/laporan') }}">LIHAT LEBIH DETAIL</a></p>
	    </div>
	</div>
	<div class="row">
	    <div class="col-lg-6 col-md-5 col-sm-5 col-xs-5">
	      <h1>Lihat Rekapitulasi</h1>
	      <p><a href="{{ url('home/rekapitulasi') }}">LIHAT LEBIH DETAIL</a></p>
	    </div>
	    <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7">
	      <picture>
	      	<img src="/img/home/tiga.png" alt="">
	      </picture>
	    </div>
	</div>
	<div class="row">
	    <div class="col-lg-6 col-md-7 col-sm-7 col-xs-7">
	      <picture>
	      	<img src="/img/home/empat.png" alt="">
	      </picture>
	    </div>
	    <div class="col-lg-6 col-md-5 col-sm-5 col-xs-5">
	      <h1>Lihat Referensi</h1>
	      <p><a href="{{ url('home/referensi') }}">LIHAT LEBIH DETAIL</a></p>
	    </div>
	</div>
</div>