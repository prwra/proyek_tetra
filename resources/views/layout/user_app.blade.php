<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="/css/style.css">

	<!-- script for link css / js
	<link rel="stylesheet" href="/css/bootstrap.css">
	<script rel="stylesheet" src="/js/bootstrap.js"></script>
	<script rel="stylesheet" src="/js/jquery.js"></script>
	-->
</head>
<body>
@section('header')
@show


@include('user.partial.profil') <!-- popup profil -->
<div class="container-fluid bg-blue">
	<div class="nav navbar-header bg-blue">
	    <button type="button" class="navbar-toggle" id="sn">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>                      
	    </button>
		<a href="{{ url('home') }}" class="navbar-brand white">TETRA</a>
    </div>
    <div class="collapse navbar-collapse bg-blue" id="myNav">
	<ul class="nav navbar-nav bg-blue">
	    <li class="active"><a href="{{ url('home/surat_tugas') }}" class="navbar-brand white">Surat Tugas</a></li>
	    <li><a href="{{ url('home/laporan') }}" class="navbar-brand white">Laporan</a></li>
	    <li><a href="{{ url('home/rekapitulasi') }}" class="navbar-brand white">Rekapitulasi</a></li>
	    <li><a href="{{ url('home/referensi') }}" class="navbar-brand white">Referensi</a></li>
	</ul>
 	<ul class="nav navbar-nav navbar-right bg-blue">
    	<li><a href="#" class="navbar-brand white" id="sp">Profil</a></li>
    	<li><a href="{{route('logout')}}" class="navbar-brand white" onclick="event.preventDefault();
    	document.getElementById('logout-form').submit();" >Logout</a></li>
    	<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
    		{{ csrf_field()}}
    	</form>
    </ul>
	</div>
</div>

<div class="container-fluid">
    @section('content')
	@show
</div>

@extends('layout.footer')

<!-- JS for profil -->
	<script type="text/javascript">
		$('#sp').on('click',function(){
			$('#profil').modal('show');
		})
	</script>
 
<!-- JS for menu
	<script type="text/javascript">
		$('#sn').on('click',function(){
			$('#myNav').modal('show');
		})
	</script>
-->

</body>
</html>