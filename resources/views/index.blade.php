<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Selamat Datang di Tetra</title>
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="/js/bootstrap.js">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>

<section>
<div class="login">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<img class="log" src="/img/login/logo.png" alt="logo tetra">
	<div class="form-group">
		<form action="{{route('login')}}" method="post">
			{{csrf_field()}}
			<input name="email" type="text" class="form-control login-btn" placeholder="NIP Anggota"><br>
			<input name="password" type="password" class="form-control login-btn" placeholder="Password"><br>
			<input type="submit" value="Login" class="btn btn-primary login-sbm">
			<br>
			<p class="white-txt">Lupa password ? <a href="" class="white-txt">Klik Disini</a></p>
		</form>
	</div>
	</div>
</div>
</section>

<picture>
	<img src="/img/login/login.png" alt="" class="bg-img">
</picture>

@extends('layout.footer')
</body>
</html>