 @extends('layout.user_app')

@section('title', 'Tetra | Detail Surat Tugas')

@section('content')
 <div class="modal fade" id="detail_surat" role="dialog">
    <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h2 class="modal-title text-center">Detail Surat</h2>
	          <hr>
	          <div class="container text-center">
				  <!--<img src="{{ asset('storage/upload/1.png')}}" alt=""> <!--dari ini-->
				  <img src="{{ asset('storage/upload/'.$l->file)}}" alt=""> <!--jadi kyk gini-->
			  </div>
	        </div>
	    </div>
	</div>
</div>
@endsection