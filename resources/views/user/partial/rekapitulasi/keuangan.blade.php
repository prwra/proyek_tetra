@extends('layout.user_app')

@section('title', 'Tetra | Rekapitulasi | Keuangan')

@section('content')
	<div class="container-fluid text-center">
		<h1 class="head"><a href="" class="head-ak">Rekapitulasi Keuangan</a></h1>
	</div>

	<div class="container-fluid blue">
		@include('sort.wilayah') <!-- sorting -->

		@include('sort.urut') <!-- sorting -->

		@include('user.partial.detail.detail_keuangan') <!-- form detial -->


		<table border="1" class="bg-blue text-center">
			<tr>
				<td width="3%" rowspan="2">No.</td>
				<td width="40%" colspan="2">Surat Tugas</td>
				<td width="14%" rowspan="2">Tujuan</td>
				<td width="5%" rowspan="2">Lama Perjalanan (Hari)</td>
				<td width="20%" rowspan="2">Biaya</td>
			</tr>
			<tr>
				<td width="25%">Nomor SPPD</td>
				<td width="15%">Tanggal Pemberian</td>
			</tr>
			<?php $no = 0;
				$total = 0;
				use App\Laporan;
			?>
			@foreach($rekap_surat as $s)
			<?php $no = $no + 1; ?>
			<?php $semen = Laporan::find($s->kode_SPPD); ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$s->kode_SPPD}}</td>
				<td>{{$s->tgl_pemberian}}</td>
				<td>{{$s->lokasi}}</td>
				<td>{{$s->lama_perjalanan}}</td>
				<td>{{$semen->biaya}}</td>
			</tr>
			<?php $total = $total + $semen->biaya; ?>
			@endforeach
			<tr>
				<td colspan="5">Jumlah Seluruh Biaya</td>
				<td>{{$total}}</td>
			</tr>
		</table>
	</div>


<!-- JS for show popup -->
<script type="text/javascript">
	$('#detail').on('click',function(){
		$('#detail_keuangan').modal('show');
	})
</script>
@endsection