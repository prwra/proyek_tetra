@extends('layout.user_app')

@section('title', 'Tetra | Rekapitulasi | Surat Tugas')

@section('content')
	<div class="container-fluid text-center">
		<h1 class="head"><a href="" class="head-ak">Rekapitulasi Surat Tugas</a>
	</div>

	<div class="container-fluid blue">
		@include('sort.wilayah') <!-- sorting -->

		@include('sort.urut') <!-- sorting -->
		
		<table border="1" class="bg-blue text-center">
			<tr>
				<td width="3%" rowspan="2">No.</td>
				<td width="45%" colspan="2">Surat Tugas</td>
				<td width="25%" colspan="2">Waktu Pelaksanaan</td>
				<td width="9%" rowspan="2">Tujuan</td>
				<td width="9%" rowspan="2">Status</td>
				<td width="9%" rowspan="2">Aksi</td>
			</tr>
			<tr>
				<td>Nomor SPPD</td>
				<td>Tanggal Pemberian</td>
				<td>Mulai</td>
				<td>Selesai</td>
			</tr>
			<?php $no = 0; ?>
			@foreach($rekap_surat as $r)
			<?php $no = $no + 1; ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$r->kode_SPPD}}</td>
				<td>{{$r->tgl_pemberian}}</td>
				<td>{{$r->tgl_mulai}}</td>
				<td>{{$r->tgl_selesai}}</td>
				<td>{{$r->lokasi}}</td>
				<td>{{$r->status}}</td>
				<td>
					<form action="surat_tugas/{{$r->kode_SPPD}}" method="POST">
						<input type="hidden" name="_method" value="delete">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input class="act-butn btn-danger" type="submit" name="submit" value="Hapus">
					</form>
				</td>
			</tr>
			@endforeach
		</table>
	</div>


<!-- JS for show popup -->
<script type="text/javascript">
	$('#detail').on('click',function(){
		$('#detail_surat').modal('show');
	})
</script>
@endsection
