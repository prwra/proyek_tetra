@extends('layout.user_app')

@section('title', 'Tetra | representasi')

@section('content')
	<div class="col-12 text-center tit">
		<p><h3>Referensi representasi</h3></p>
	</div>
	
	<div class="container-fluid blue">
	@include('sort.urut_nama')
	
	

		<table border="1" class="bg-blue text-center" style="width:100%;">
			<tr>
				<th width="3%">No.</th>

				<th width="17%">Wilayah</th>
				<th width="10%">II A</th>
				<th width="10%">II B</th>
				<th width="10%">III A</th>
				<th width="10%">III B</th>
				<th width="10%">IV A</th>
				<th width="10%">IV B</th>

			</tr>
			<?php $no = 0 ?>
			@foreach($representasi as $p)
			<?php $no = $no + 1; ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$p->wilayah}}</td>
				<td>{{$p->II_A}}</td>
				<td>{{$p->II_B}}</td>
				<td>{{$p->III_A}}</td>
				<td>{{$p->III_B}}</td>
				<td>{{$p->IV_A}}</td>
				<td>{{$p->IV_B}}</td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection
