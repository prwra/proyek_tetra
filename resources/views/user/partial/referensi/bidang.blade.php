@extends('layout.user_app')

@section('title', 'Tetra | Bidang')

@section('content')
	<div class="col-12 text-center tit">
		<p><h3>Referensi Bidang</h3></p>
	</div>
	
	<div class="container-fluid blue">
	@include('sort.urut_nama')
	
	

		<table border="1" class="bg-blue text-center" style="width:100%;">
			<tr>
				<th width="3%">No.</th>
				<th width="50%">Bidang</th>
			</tr>
			<?php $no = 0 ?>
			@foreach($bidang as $p)
			<?php $no = $no + 1; ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$p->bidang}}</td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection
