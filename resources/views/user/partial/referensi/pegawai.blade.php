@extends('layout.user_app')

@section('title', 'Tetra | Pegawai')

@section('content')
	<div class="col-12 text-center tit">
		<p><h3>Referensi Pegawai</h3></p>
	</div>
	
	<div class="container-fluid blue">
	@include('sort.urut_nama')
	@include('admin.form.tambah_pegawai_form')
	

		<table border="1" class="bg-blue text-center" style="width:100%;">
			<tr>
				<th width="3%">No.</th>
				<th width="8%">NIP</th>
				<th width="17%">Nama</th>
				<th width="10%">Jenis Kelamin</th>
				<th width="15%">Alamat</th>
				<th width="10%">Jabatan</th>
				<th width="10%">Eselon</th>
				<th width="10%">Golongan</th>
			</tr>
			<?php $no = 0 ?>
			@foreach($pengguna as $p)
			<?php $no = $no + 1; ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$p->nip}}</td>
				<td>{{$p->nama}}</td>
				<td>{{$p->kelamin}}</td>
				<td>{{$p->alamat}}</td>
				<td>{{$p->jabatan}}</td>
				<td>{{$p->eselon}}</td>
				<td>{{$p->golongan}}</td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection
