@extends('layout.user_app')

@section('title', 'Tetra | Referensi')

@section('content')
	<div class="col-12 text-center tit">
		<p><h3>Referensi Apa Yang Ingin Anda Lihat?</h3></p>
	</div>
	<div class="container-fluid text-center">
		<div class="row">
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/pegawai') }}">
			    <picture>
			    	<img src="/img/icon/1Pegawai.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Pegawai</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/pangkat') }}">
			    <picture>
			    	<img src="/img/icon/2Pangkat.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Pangkat</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/jabatan') }}">
			    <picture>
			    	<img src="/img/icon/3Jabatan.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Jabatan</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/eselon') }}">
			    <picture>
			    	<img src="/img/icon/4Eselon.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Eselon</label></p>
		    </div>
		</div><br>
		<div class="row">
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/bidang') }}">
			    <picture>
			    	<img src="/img/icon/5Bidang.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Bidang</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/wilayah') }}">
			    <picture>
			    	<img src="/img/icon/6Wilayah.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Wilayah</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/hotel') }}">
			    <picture>
			    	<img src="/img/icon/9Hotel.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Hotel</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/transportasi') }}">
			    <picture>
			    	<img src="/img/icon/10Transportasi.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Transportasi</label></p>
		    </div>
		</div>
		<div class="row">
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="">
			    <picture>
			    	<img src="/img/icon/7UangRepresentasi.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Uang Representasi</label></p>
		    </div>
		    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ref">
			    <a href="{{ url('home/referensi/harian') }}">
			    <picture>
			    	<img src="/img/icon/8UangHarian.png" alt="" class="img-ref">
			    </picture>
				</a>
			    <p class="text-center"><label for="">Uang Harian</label></p>
		    </div>
		</div>
	</div>
@endsection

