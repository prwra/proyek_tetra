@extends('layout.user_app')

@section('title', 'Tetra | Laporan')

@section('content')
	<div class="container-fluid text-center">
		<h1 class="head"><a href="#" class="head-ak">Laporan</a></h1>
	</div>

	<div class="container-fluid bg-head text-center">
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo numquam recusandae at molestiae officia, odio quae id provident! Laudantium quos, ea rem, animi soluta odit nulla natus repellendus blanditiis. Ducimus!
	</p>
	</div>

	<div class="container-fluid blue">
		@include('sort.wilayah') <!-- sorting -->

		@include('sort.urut') <!-- sorting -->

		@include('user.form.tambah_laporan_form') <!-- form tambah laporan -->

		@include('user.partial.detail.detail_laporan') <!-- form detail -->

		<button class="navbar-right butn" id="add">
			Tambah Laporan
		</button>

		<table border="1" class="bg-blue text-center">
			<tr>
				<td width="3%" rowspan="2">No.</td>
				<td width="40%" colspan="2">Surat Tugas</td>
				<td width="10%" rowspan="2">Biaya</td>
				<td width="20%" rowspan="2">File</td>
				<td width="9%" rowspan="2">Tujuan</td>
				<td width="9%" rowspan="2">Status</td>
				<td width="9%" rowspan="2">Aksi</td>
			</tr>
			<tr>
				<td width="25%">Nomor SPPD</td>
				<td width="15%">Tanggal Pemberian</td>
			</tr>
			<?php $no = 0;
				use App\Rekap_Surat_Tugas;
			?>
			@foreach($laporan as $l)
			<?php $no = $no + 1; ?>
			<?php $semen = Rekap_Surat_Tugas::find($l->kode_laporan); ?>
			<tr>
				<td>{{$no}}</td>
				<td>{{$l->kode_laporan}}</td>
				<td>{{$semen->tgl_pemberian->format('d F Y')}}</td>
				<td>{{$l->biaya}}</td>
				<td>{{"kosong"}}</td>
				<td>{{$semen->lokasi}}</td>
				<td>{{$semen->status}}</td>
				<td>
					<!--<button class="butn-det" id="detail">
						Detail
					</button>-->
					<form method="get" action="laporan/{{$l->kode_laporan}}/edit">
					    <button type="submit" class="act-butn btn-warning" id="edt">Ubah</button>
					</form>
					<form action="laporan/{{$l->kode_laporan}}" method="POST">
						<input type="hidden" name="_method" value="delete">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input class="act-butn btn-danger" type="submit" name="submit" value="Hapus">
					</form>
				</td>
			</tr>
			@endforeach
		</table>
	</div>

<!-- JS for show popup -->
<script type="text/javascript">
	$('#add').on('click',function(){
		$('#tambah_laporan').modal('show');
	})
	$('#detail').on('click',function(){
		$('#detail_laporan').modal('show');
	})
</script>
@endsection
