<?php

Route::get('/', function() {
	return redirect ('/login');
});

// Routes for admin --------
Route::get('/admin', 'PageAdminController@surat');

//Route::get('/admin/surat_tugas', 'PageAdminController@surat');

//Route::get('/admin/laporan', 'PageAdminController@laporan');

Route::get('/admin/rekapitulasi', 'PageAdminController@rekapitulasi');
	// Routes for content of rekapitulasi
	//Route::get('/admin/rekapitulasi/surat_tugas', 'PageAdminController@rekapitulasi_surat');
	Route::get('/admin/rekapitulasi/perjalanan', 'PageAdminController@rekapitulasi_perjalanan');
	//Route::get('/admin/rekapitulasi/keuangan', 'PageAdminController@rekapitulasi_keuangan');

Route::get('/admin/referensi', 'PageAdminController@referensi');

Route::resource('admin/referensi/pegawai', 'PenggunaController');
Route::resource('admin/surat_tugas', 'Surat_TugasController');
Route::resource('admin/laporan', 'LaporanController');
Route::resource('admin/rekapitulasi/surat_tugas', 'Rekap_SuratController');
Route::resource('admin/rekapitulasi/keuangan', 'Rekap_UangController');

// End routes for admin --------


// Routes for users --------
// Routes for paging
Route::get('/home', 'PageController@home');

//Route::get('/home/surat_tugas', 'PageController@surat');

//Route::get('/home/laporan', 'PageController@laporan');

Route::get('/home/rekapitulasi', 'PageController@rekapitulasi');
	// Routes for content of rekapitulasi
	//Route::get('/home/rekapitulasi/surat_tugas', 'PageController@rekapitulasi_surat');
	Route::get('/home/rekapitulasi/perjalanan', 'PageController@rekapitulasi_perjalanan');
	//Route::get('/home/rekapitulasi/keuangan', 'PageController@rekapitulasi_keuangan');

Route::get('/home/referensi', 'PageController@referensi');

Route::resource('home/referensi/pegawai', 'PenggunaUController');
Route::resource('home/surat_tugas', 'Surat_TugasUController');
Route::resource('home/laporan', 'LaporanUController');
Route::resource('home/rekapitulasi/surat_tugas', 'Rekap_SuratUController');
Route::resource('home/rekapitulasi/keuangan', 'Rekap_UangUController');
Route::resource('home/referensi/pangkat', 'PangkatUController');
Route::resource('home/referensi/jabatan', 'JabatanUController');
Route::resource('home/referensi/eselon', 'EselonUController');
Route::resource('home/referensi/hotel', 'HotelUController');
Route::resource('home/referensi/bidang', 'BidangUController');
Route::resource('home/referensi/wilayah', 'WilayahUController');
Route::resource('home/referensi/transportasi', 'TransportasiUController');
Route::resource('home/referensi/representasi', 'RepresentasiUController');
Route::resource('home/referensi/harian', 'HarianUController');
// Routes for process
Route::get('/home/surat_tugas/tambah_surat_form', 'HomeController@index');
// End routes for users --------
//form
Route::resource('pengguna', 'PenggunaController');
Route::resource('surat_tugas', 'Surat_TugasController');
Route::resource('surat_tugasU', 'Surat_TugasUController');
Route::resource('laporan', 'LaporanController');
Route::resource('laporanU', 'LaporanUController');
Route::resource('rekap_surat', 'Rekap_SuratController');
Route::resource('rekap_surat', 'Rekap_SuratUController');
Route::resource('rekap_uang', 'Rekap_UangController');
Route::resource('rekap_uang', 'Rekap_UangController');
Route::resource('pangkat', 'PangkatUController');
Route::resource('jabatan', 'JabatanUController');
Route::resource('eselon', 'EselonUController');
Route::resource('hotel', 'HotelUController');
Route::resource('wilayah', 'WilayahUController');
Route::resource('transportasi', 'TransportasiUController');
Route::resource('representasi', 'RepresentasiUController');
Route::resource('harian', 'HarianUController');
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');