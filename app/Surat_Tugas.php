<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat_Tugas extends Model
{
    protected $table = 'surat_tugas';
    protected $primaryKey = 'kode_SPPD';
    protected $dates = ([
    	'tgl_pemberian', 'tgl_mulai', 'tgl_selesai',
    ]);
}
