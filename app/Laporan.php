<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    protected $table = 'laporan';
    protected $primaryKey = 'kode_laporan';
    protected $dates = ([
    	'tgl_upload',
    ]);
}
