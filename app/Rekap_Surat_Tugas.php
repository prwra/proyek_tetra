<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekap_Surat_Tugas extends Model
{
    protected $table = 'Rekap_Surat_Tugas';
    protected $primaryKey = 'kode_SPPD';
    protected $dates = ([
    	'tgl_pemberian', 'tgl_mulai', 'tgl_selesai',
    ]);
}
