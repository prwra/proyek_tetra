<?php

namespace App\Http\Controllers;

use App\Surat_Tugas;
use Illuminate\Http\Request;

class Surat_TugasController extends Controller
{


     public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['surat_tugas'] = Surat_Tugas::all();
        return view('admin.surat_tugas', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah_surat_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_SPPD' => 'required',
            'tgl_pemberian' => 'required',
            'lokasi' => 'required'
        ]);
        $surat = new Surat_Tugas;
        $surat->kode_SPPD = $request->kode_SPPD;
        $surat->tgl_pemberian = $request->tgl_pemberian;
        $surat->tgl_mulai = $request->tgl_mulai;
        $surat->tgl_selesai = $request->tgl_selesai;
        $surat->lama_perjalanan = $request->lama_perjalanan;
        $surat->lokasi = $request->lokasi;
        $surat->file = $request->file;
        $surat->status = "Belum diterima";

        $surat->save();
        return redirect('admin/surat_tugas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = Surat_Tugas::find($id);
        
        //$pengguna = pengguna::where(['nip'=>$id])->get(); 
        return view('admin.form.edit_surat_form', compact('s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode_SPPD' => 'required',
            'tgl_pemberian' => 'required',
            'lokasi' => 'required'
        ]);
        $surat = Surat_Tugas::find($id);
        $surat->kode_SPPD = $request->kode_SPPD;
        $surat->tgl_pemberian = $request->tgl_pemberian;
        $surat->tgl_mulai = $request->tgl_mulai;
        $surat->tgl_selesai = $request->tgl_selesai;
        $surat->lama_perjalanan = $request->lama_perjalanan;
        $surat->lokasi = $request->lokasi;
        $surat->file = $request->file;
        $surat->status = "Belum diterima";

        $surat->save();
        return redirect('admin/surat_tugas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $surat = Surat_Tugas::find($id);
        if ($surat != null) {
            $surat->delete();
            return redirect('admin/surat_tugas')->with(['message'=> 'Successfully deleted!!']);
        }

        return redirect('admin/surat_tugas')->with(['message'=> 'Wrong ID!!']);
    }
}
