<?php

namespace App\Http\Controllers;
use App\Surat_Tugas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Surat_TugasUController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['surat_tugas'] = Surat_Tugas::all();
        return view('user.surat_tugas', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah_surat_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_SPPD' => 'required',
            'tgl_pemberian' => 'required',
            'lokasi' => 'required',
            'file' => 'required'
        ]);
        $surat = new Surat_Tugas;
        if($request->hasFile('file')){
            //$filename = $request->file->getClientOriginalName();
            $filename = $request->kode_SPPD;
            $surat->file = $filename.'.jpg';
            $request->file->storeAs('public/upload', $filename.'.jpg');
        }
            $surat->kode_SPPD = $request->kode_SPPD;
            $surat->tgl_pemberian = $request->tgl_pemberian;
            $surat->tgl_mulai = $request->tgl_mulai;
            $surat->tgl_selesai = $request->tgl_selesai;
            $surat->lama_perjalanan = $request->lama_perjalanan;
            $surat->lokasi = $request->lokasi;
            $surat->status = "Belum diterima";

            $surat->save();
            return redirect('home/surat_tugas');
        /*
        */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $s = Surat_Tugas::find($id);
        
        //$pengguna = pengguna::where(['nip'=>$id])->get(); 
        return view('user.form.edit_surat_form', compact('s'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = Surat_Tugas::find($id);
        
        //$pengguna = pengguna::where(['nip'=>$id])->get(); 
        return view('user.form.edit_surat_form', compact('s'));
    }
     public function lihat($id)
    {
        $l = Surat_Tugas::find($id);
        
        //$pengguna = pengguna::where(['nip'=>$id])->get(); 
        return view('user.partial.detail.detail_surat', compact('l'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode_SPPD' => 'required',
            'tgl_pemberian' => 'required',
            'lokasi' => 'required'
        ]);
        $surat = Surat_Tugas::find($id);
        if($request->hasFile('file')){
            $filename = $request->kode_SPPD.'.jpg';
            Storage::delete($surat->file);
            $request->file->storeAs('public/upload', $filename);
            $surat->file = $filename;
        //<input type="file" accept=".png, .jpg, .jpeg" />
        }
            $surat->kode_SPPD = $request->kode_SPPD;
            $surat->tgl_pemberian = $request->tgl_pemberian;
            $surat->tgl_mulai = $request->tgl_mulai;
            $surat->tgl_selesai = $request->tgl_selesai;
            $surat->lama_perjalanan = $request->lama_perjalanan;
            $surat->lokasi = $request->lokasi;
            $surat->status = "Belum diterima";

            $surat->save();
            return redirect('home/surat_tugas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $surat = Surat_Tugas::find($id);
        if ($surat != null) {
            $surat->delete();
            return redirect('home/surat_tugas')->with(['message'=> 'Successfully deleted!!']);
        }

        return redirect('home/surat_tugas')->with(['message'=> 'Wrong ID!!']);
    }
}
