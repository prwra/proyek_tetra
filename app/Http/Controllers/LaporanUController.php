<?php

namespace App\Http\Controllers;
use App\Laporan;
use App\Surat_Tugas;
use App\Rekap_Surat_Tugas;
use Illuminate\Http\Request;

class LaporanUController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['laporan'] = Laporan::all();
        return view('user.laporan', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah_laporan_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'biaya' => 'required',
            'kode_SPPD' => 'required'
        ]);
        $laporan = new Laporan;
        $laporan->kode_Laporan = $request->kode_SPPD;
        $laporan->tgl_upload = date('Y-m-d');
        $laporan->biaya = $request->biaya;
        $laporan->kode_SPPD = $request->kode_SPPD;
        //$laporan->file = $request->file;
        $laporan->save();
        $semen = Surat_Tugas::find($request->kode_SPPD);
        $rekap_surat = new Rekap_Surat_Tugas;
        $rekap_surat->kode_SPPD = $semen->kode_SPPD;
        $rekap_surat->tgl_pemberian = $semen->tgl_pemberian;
        $rekap_surat->tgl_mulai = $semen->tgl_mulai;
        $rekap_surat->tgl_selesai = $semen->tgl_selesai;
        $rekap_surat->lama_perjalanan = $semen->lama_perjalanan;
        $rekap_surat->lokasi = $semen->lokasi;
        $rekap_surat->file = $semen->file;
        $rekap_surat->status = "Sudah diterima";
        $rekap_surat->save();
        $semen->delete();
        return redirect('home/laporan');
        //redirect('laporan/$laporan->kode_SPPD/del');
        //return redirect()->action('LaporanController@del', ['id' => $laporan->kode_SPPD]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $l = Laporan::find($id);
        
        //$pengguna = pengguna::where(['nip'=>$id])->get(); 
        return view('user.form.edit_laporan_form', compact('l'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'biaya' => 'required',
            'kode_SPPD' => 'required'
        ]);
        $laporan = Laporan::find($id);
        $laporan->kode_Laporan = $request->kode_SPPD;
        $laporan->tgl_upload = date('Y-m-d');
        $laporan->biaya = $request->biaya;
        $laporan->kode_SPPD = $request->kode_SPPD;
        //$laporan->file = $request->file;

        $laporan->save();
        return redirect('home/laporan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $laporan = Laporan::find($id);
        $rekap_surat = Surat_Tugas::find($id);
        if ($laporan != null) {
            $laporan->delete();
            return redirect('home/laporan')->with(['message'=> 'Successfully deleted!!']);
        }

        return redirect('home/laporan')->with(['message'=> 'Wrong ID!!']);
    }
}
