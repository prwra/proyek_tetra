<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(){
    	return view('index');
    }
    
    public function surat(){
    	return view('admin.surat_tugas');
    }
    public function usulan(){
        return view('admin.usulan');
    }

    public function laporan(){
        return view('admin.laporan');
    }

    public function rekapitulasi(){
        return view('admin.rekapitulasi');
    }
    // controllers for content of rekapitulasi
        public function rekapitulasi_surat(){
            return view('admin.partial.rekapitulasi.surat_tugas');
        }
        public function rekapitulasi_perjalanan(){
            return view('admin.partial.rekapitulasi.perjalanan');
        }
        public function rekapitulasi_keuangan(){
            return view('admin.partial.rekapitulasi.keuangan');
        }

    public function referensi(){
    	return view('admin.referensi');
    }

}
