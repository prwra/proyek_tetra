<?php

namespace App\Http\Controllers;
use App\Rekap_Surat_Tugas;
use App\Surat_Tugas;
use App\Laporan;
use Illuminate\Http\Request;

class Rekap_SuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['rekap_surat'] = Rekap_Surat_Tugas::all();
        return view('admin.partial.rekapitulasi.surat_tugas', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rekap_surat = Rekap_Surat_Tugas::find($id);
        $laporan = Laporan::find($id);
        if ($rekap_surat != null) {
            $rekap_surat->delete();
            $laporan->delete();
            return redirect('admin/rekapitulasi/surat_tugas')->with(['message'=> 'Successfully deleted!!']);
        }
        return redirect('admin/rekapitulasi/surat_tugas')->with(['message'=> 'Wrong ID!!']);
    }
}
