<?php

namespace App\Http\Controllers;

use App\Pengguna;
use Illuminate\Http\Request;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pengguna'] = Pengguna::all();
        return view('admin.partial.referensi.pegawai', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah_pegawai_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'eselon' => 'required',
            'golongan' => 'required'
        ]);
        $pengguna = new pengguna;
        $pengguna->nip = $request->nip;
        $pengguna->nama = $request->nama;
        $pengguna->tempat_lahir = $request->tmpt_lahir;
        $pengguna->tgl_lahir = $request->tgl_lahir;
        $pengguna->kelamin = $request->jns_kel;
        $pengguna->alamat = $request->alamat;
        $pengguna->jabatan = $request->jabatan;
        $pengguna->eselon = $request->eselon;
        $pengguna->golongan = $request->golongan;

        $pengguna->save();
        return redirect('admin/referensi/pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $p = pengguna::find($id);
        
        //$pengguna = pengguna::where(['nip'=>$id])->get(); 
        return view('admin.form.edit_pegawai_form', compact('p'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nip' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'eselon' => 'required',
            'golongan' => 'required'
        ]);
        
        $pengguna = pengguna::find($id);
        //$pengguna = pengguna::where(['nip'=>$id])->get();
        $pengguna->nip = $request->nip;
        $pengguna->nama = $request->nama;
        $pengguna->tempat_lahir = $request->tmpt_lahir;
        $pengguna->tgl_lahir = $request->tgl_lahir;
        $pengguna->kelamin = $request->jns_kel;
        $pengguna->alamat = $request->alamat;
        $pengguna->jabatan = $request->jabatan;
        $pengguna->eselon = $request->eselon;
        $pengguna->golongan = $request->golongan;

        $pengguna->save();
        return redirect('admin/referensi/pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengguna = pengguna::find($id);
        if ($pengguna != null) {
            $pengguna->delete();
            return redirect('admin/referensi/pegawai')->with(['message'=> 'Successfully deleted!!']);
        }

        return redirect('admin/referensi/pegawai')->with(['message'=> 'Wrong ID!!']);
    }
}
