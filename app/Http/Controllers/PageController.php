<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{   

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
    	return view('index');
    }

    public function home(){
    	return view('user.home');
    }

    public function surat(){
    	return view('user.surat_tugas');
    }
    public function usulan(){
        return view('user.usulan');
    }

    public function laporan(){
        return view('user.laporan');
    }

    public function rekapitulasi(){
        return view('user.rekapitulasi');
    }
    // controllers for content of rekapitulasi
        public function rekapitulasi_surat(){
            return view('user.partial.rekapitulasi.surat_tugas');
        }
        public function rekapitulasi_perjalanan(){
            return view('user.partial.rekapitulasi.perjalanan');
        }
        public function rekapitulasi_keuangan(){
            return view('user.partial.rekapitulasi.keuangan');
        }

    public function referensi(){
    	return view('user.referensi');
    }

}
